// khai báo thư viện express
const { request, response } = require('express');
const express = require('express');
const path = require('path');
// export router
const router = require('./app/Router/singInRouter');
//khởi tạo app
const app = express();

// khởi tạo cổng chạy app
const port = 8000;

app.use('/', router); 
// khai bao suw dung tai nguyen
app.use(express.static('link'));
// khai bao API dang get chay vao day
app.get('/', (request, response)=>{
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/link/Pleasesignin.html'));
})

app.listen(port, ()=>{
    console.log("app chạy trên cổng : " + port);
})
